from locust import HttpLocust, TaskSet


class StringsFunTasks(TaskSet):
    def login(self):
        response = self.client.post("/login", {"username": "testuser", "password": "password"})
        return response.status_code, response.content

    def welcome_page(self):
        response = self.client.get("/")
        return response.status_code, response.content

    def statistics_page(self):
        response = self.client.get("/stats")
        return response.status_code, response.content

    def get_random(self):
        response = self.client.get("/random")
        return response.status_code, response.content

    def get_joke(self):
        self.client.post("/joke")

    def get_named_joke(self):
        self.client.post("/joke/Ihor Tanyenkov")

    def get_wiki(self):
        self.client.post('/wiki/Bill Clinton')

    def get_spelling(self):
        self.client.post('/spelling/ramdom')


class WebsiteUser(HttpLocust):
    task_set = StringsFunTasks
    min_wait = 5000
    max_wait = 9000
