from flask import Flask

app = Flask(__name__)

TESTING = False
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND ='redis://localhost:6379/0'
SECRET_KEY = 'urban'
SQLALCHEMY_DATABASE_URI ='sqlite:///db.sqlite'
SQLALCHEMY_COMMIT_ON_TEARDOWN = True

RANDOM_WORD_URL = 'http://setgetgo.com/randomword/get.php'
WIKI_API_URL = "https://en.wikipedia.org/w/api.php?format=json&action="
CHUCK_API = 'http://api.icndb.com/jokes/random'
WIKI_API_QUERY = 'query&prop=extracts&exintro=&explaintext=&titles='
SPELLCHECK = 'https://montanaflynn-spellcheck.p.mashape.com/check/?text='
SPELLCHECK_KEY = 'S8TBQB2XozmshAVbq5WPkgypRSAXp1OikPtjsnMfCGfY8DpW4m'
