#!/usr/bin/env bash

# Dependencies:
# virtualenv
# python3-pip
# redis-server

python_env_activate() {
if [ ! -d venv ]; then
    virtualenv -p $(which python3) venv
fi
source venv/bin/activate
echo "venv activated"
}


if [ $# -ge 1 ]
then
for i in "$@"
do
case $i in
    -h | --help)
        echo "-h, --help      :Commands help."
        echo "-i, --install   :Install all dependencies from requirements.txt."
        echo "-r, --run       :Run application."
        echo "-w, --worker    :Run celery worker."
        echo "-p, --perftest  :Run performance tests"
    ;;
    -i | --install)
        python_env_activate
        pip3 install -r requirements.txt
    ;;

    -r | --run)
        python_env_activate
        python3  stringsfun.py
    ;;
    -w | --worker)
        python_env_activate
        celery worker -A stringsfun.celery --loglevel=debug
    ;;
    -p | --perftest)
        locust -f tests/locustfile.py --csv=foobar --no-web -n10 -c100
    ;;
    *)
      echo "Unknown option"
    ;;
esac
done
else
    sudo apt-get --assume-yes install redis-server
    sudo  pip3 install git+https://github.com/locustio/locust.git
    python_env_activate
    pip3 install -r requirements.txt
    celery worker -A stringsfun.celery --loglevel=debug &
    python3  stringsfun.py

fi
