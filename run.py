from __future__ import absolute_import, unicode_literals
from stringsfun import app

# app.run(debug=True, port=9001)

from celery import current_app
from celery.bin import worker

application = current_app._get_current_object()

worker = worker.worker(app=application)

options = {
    'broker': app.config['CELERY_BROKER_URL'],
    'backend': app.config['CELERY_RESULT_BACKEND'],
    'loglevel': 'debug',
    'traceback': True,
}

worker.run(**options)
