import json
import os
import requests
import bcrypt
import logging
import config

from celery import Celery
from flask import Flask, jsonify, request, abort, g
from flask_httpauth import HTTPBasicAuth
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

from logging.handlers import RotatingFileHandler
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)

app = Flask(__name__)

app.config['TESTING'] = config.TESTING
app.config['CELERY_BROKER_URL'] = config.CELERY_BROKER_URL
app.config['CELERY_RESULT_BACKEND'] = config.CELERY_RESULT_BACKEND
app.config['SECRET_KEY'] = config.SECRET_KEY
app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = config.SQLALCHEMY_COMMIT_ON_TEARDOWN

login_manager = LoginManager()
login_manager.init_app(app)

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'],
                backend=config.CELERY_RESULT_BACKEND)
celery.conf.update(app.config)

db = SQLAlchemy(app)
auth = HTTPBasicAuth()

STATS = 'stats.json'


class User(db.Model):
    """
    User class for registration
    """
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True)
    password_hash = db.Column(db.String(128))

    def hash_password(self, password):
        salt = bcrypt.gensalt()
        self.password_hash = bcrypt.hashpw(password.encode('utf-8'), salt)

    def verify_password(self, password):
        return bcrypt.checkpw(password.encode('utf-8'), self.password_hash)

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        serializer = Serializer(app.config['SECRET_KEY'])
        try:
            data = serializer.loads(token)
        except (SignatureExpired, BadSignature):
            return
        user = User.query.get(data['id'])
        return user


class Consumer(object):
    """ Base communication class.
    Includes methods to call external API`s and parse results
    """
    def __init__(self):
        self.connection_timeout = 30
        self.read_timeout = 30
        self.headers = {'content-type': 'application/json'}
        self.stat_file = 'stats.json'

    @staticmethod
    def read_stat(file: str) -> dict:
        data = open(file).read()
        stats = json.loads(data)
        return stats

    def collect_stats(self, user, keyword):
        """Write user stats to json file
        """
        stats = self.read_stat(STATS)
        if user in stats.keys():
            if keyword in stats[user].keys():
                stats[user][keyword] += 1
            else:
                stats[user][keyword] = 1
        else:
            stats[user] = {keyword: 1}
        with open('stats.json', 'w') as f:
            json.dump(stats, f)

    @staticmethod
    def __get_headers() -> dict:
        return {'content-type': 'application/json'}

    @staticmethod
    def __parse_article(data: dict) -> str:
        """Parse data from from wiki API`s represented as nested json.
        :param data: data received from Wiki API`.
        """
        article = data['query']['pages']
        for i in article:
            return article[i].get('extract')

    @staticmethod
    def __call_external_api(url, headers=None):
        """
        Universal method. Send requests to external resources
        :param url: resource URL
        :param headers: Default headers
        :return: response object
        """
        try:
            response = requests.get(url, headers=headers)
            if response.status_code // 100 != 2:
                return response.status_code, response.reason
            return response
        except requests.exceptions.RequestException as e:  # trying to handle network issues
            return 'Error {}'.format(e)

    def get_wiki(self, keyword):
        """ fetch wiki data"""
        data = self.__call_external_api(config.WIKI_API_URL + config.WIKI_API_QUERY + keyword)
        result = self.__parse_article(data.json())
        return result

    def get_random(self):
        """ get random word from API"""
        result = self.__call_external_api(config.RANDOM_WORD_URL)
        return result

    def splellcheck(self, text: str) -> str:
        """
        Performs spellchecking on input sentence using external resource
        :param text: input  string ENG language only
        :return: string: returns string with suggestion
        """
        result = self.__call_external_api(config.SPELLCHECK + text, headers={
            "X-Mashape-Key": config.SPELLCHECK_KEY,
            "Accept": "application/json"
        })
        suggested = result.json()['suggestion']
        return suggested


consumer = Consumer()


def __parse_joke(data: dict) -> str:
    """Parsing Chuck Norris jokes
    :param data:
    :return: joke string
    """

    joke = data['value']['joke']
    return joke


def __parse_name(name: str) -> list:
    """Parse name for Chuck Norris jokes API
    :param name:
    :return: list of firs and last name
    """
    app.logger.info('PARSING NAME: %s' % name)
    name = str(name).split(' ')
    if len(name) >= 2:
        return name[:2]
    elif len(name) == 1:
        name.append(' ')
        return name


@celery.task
def get_chuck(name: str) -> str:
    """
    Get random Chuck Norris joke, if name is not defined.
    If name is defined returns joke with defined name instead of Chuck Norris
    :param name: First name, last name optionally
    :return: json with joke
    """
    if name:
        name = __parse_name(name)
        api_query = '?firstName={first}&lastName={last}'.format(first=name[0],
                                                                last=name[1])
        result = requests.get(url=config.CHUCK_API + api_query)
        data = result.json()
        joke = data['value']['joke']
        return joke
    data = result = requests.get(url=config.CHUCK_API)
    joke = __parse_joke(data.json())
    return joke


@app.route('/register', methods=['POST'])
def register():
    username = request.json.get('username')
    password = request.json.get('password')
    if username is None or password is None:
        abort(400)
    if User.query.filter_by(username=username).first() is not None:
        abort(400)
    user = User(username=username)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return jsonify({'username': user.username}), 201


@auth.verify_password
def verify_password(username_or_token, password):
    user = User.verify_auth_token(username_or_token)
    if not user:
        user = User.query.filter_by(username=username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True


@app.route('/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token(600)
    return jsonify({'token': token.decode('ascii'), 'duration': 600})


@celery.task
def get_random_word():
    word = consumer.get_random()
    return word.text


@app.route('/random', methods=['GET'])
@auth.login_required
def random():
    app.logger.info('GET')
    response = get_random_word.apply_async()
    return response.wait(), 200


@app.route('/wiki/<keyword>', methods=['POST'])
@auth.login_required
def wiki(keyword):
    if keyword:
        username = g.user.username
        consumer.collect_stats(username, keyword)
        article = consumer.get_wiki(keyword)
        return jsonify(article)
    app.logger.error('BAD REQUEST: %s' % request.get_json())
    abort(400)


@app.route('/joke', defaults={'name': None}, methods=['POST'])
@app.route('/joke/<name>', methods=['POST'])
@auth.login_required
def chuck_joke(name):
    if name:
        response = get_chuck.apply_async(args=[name])
        return response.wait(), 200
    elif not name:
        response = get_chuck.apply_async(args=[None])
        return jsonify(response.wait()), 200
    app.logger.error('BAD REQUEST: %s' % request.get_json())
    abort(400)


@app.route('/', methods=['GET'])
@auth.login_required
def greeting():
    username = g.user.username
    return 'welcome {}'.format(username)


@app.route('/spelling/<sentence>', methods=['POST'])
@auth.login_required
def spelling(sentence):
    if sentence:
        suggested = consumer.splellcheck(sentence)
        return jsonify(suggested)
    app.logger.error('BAD REQUEST: %s' % request.get_json())
    abort(400)


@app.route('/stats', methods=['GET'])
@auth.login_required
def stats():
    data = consumer.read_stat(STATS)
    return jsonify(data)


if __name__ == '__main__':
    if not os.path.exists('db.sqlite'):
        db.create_all()
    handler = RotatingFileHandler('stringsfun.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]'
    ))
    app.logger.addHandler(handler)
    app.run(debug=True)
